<?php
/**
 * The template for displaying all single posts and attachments

 */
 
get_header('shop'); ?>

<section class="recentPost">
    <div class="container">
        <main class="site-main">

            <?php
        // Start the loop.
        while ( have_posts() ) : the_post(); ?>
            <div class="entry_line">
                <h1 class="titlePost mb-55"><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>



            <?php /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
            get_template_part( 'content', get_post_format() );
 
            // If comments are open or we have at least one comment, load up the comment template.
            /*if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif; */
 
            // Previous/next post navigation.
            the_post_navigation( array(
                'next_text' =>
                    '<span class="screen-reader-text">' . __( 'Nowszy wpis', 'twentyfifteen' ) . '</span> ' .            
                    '&raquo;',
                'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( '&laquo;', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Starszy wpis', 'twentyfifteen' ) . '</span> ',

            ) );

        // End the loop.
        endwhile;
        ?>

        </main><!-- .site-main -->
    </div><!-- .container -->
</section>




<?php get_footer(); ?>