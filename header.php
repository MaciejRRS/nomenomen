<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?php wp_title(); ?>
    </title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
    <?php if (is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply'); ?>


    <?php wp_head();
    global $woocommerce; ?>
</head>

<body <?php body_class(); ?>>
    <header>
        <div class="container">
            <div class="flex">
                <?php
                $custom_logo_id = get_theme_mod('custom_logo');
                $image = wp_get_attachment_image_src($custom_logo_id, 'full'); ?>
                <a class="navbar-brand logo-mobile" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                    <img src="<?php echo $image[0]; ?>" alt="<?php bloginfo('name'); ?>">
                </a>
                <div id="mobile_nav" class="mobile_nav_button"><span></span></div>
                <nav id="navigation">
                    <?php
                    wp_nav_menu(array(
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav navbar-nav-primary',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker(),
                    ));
                    ?>
                </nav>
            </div>
        </div>
    </header>

    <script>
        let button = document.getElementById('mobile_nav')
        let menu = document.getElementById('navigation')
        button.addEventListener('click', () => {
            if(button.classList.contains('active')){
                button.classList.remove('active')
                navigation.classList.remove('active')
            } else{
                button.classList.add('active')
                navigation.classList.add('active')
            }
        })
    </script>