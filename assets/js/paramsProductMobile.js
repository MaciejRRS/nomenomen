$(document).ready(function () {
    if (document.getElementById('family-name') && window.innerWidth < 992) {

        const item = document.getElementById('variations_form')
        const button = document.getElementById('family-name')

        const changeFilterState = () => {
            if (item.classList.contains('active')) {
                item.classList.remove('active')
            } else {
                item.classList.add('active')
            }
        }

        button.addEventListener('click', () => { changeFilterState() })
    }
})
