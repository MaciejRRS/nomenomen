$(document).ready(function () {
    let $ancestorDiv = $('select').closest('.wrap-filter');
    $('.orderby').select2({
        minimumResultsForSearch: Infinity,
        dropdownParent: $ancestorDiv,
        width: 'resolve'
    });

});

