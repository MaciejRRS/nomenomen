//or however you get a handle to the IMG
function imgPercentage() {
    const img = [...document.querySelectorAll('.link-hover-project .wrap-image-apla .homepage-item-image.desktop, .link-hover-product .homepage-item-image.desktop, .blocks.projects .grid-box .homepage-item-image.desktop')];
    const gridBox = document.querySelector('.grid-box');
    img.map(img => {
        let imageWidth = img.clientWidth;
        let imageHeight = img.clientHeight;
        let gridBoxWidth = gridBox.clientWidth;
        let percentage = ((imageWidth * 100) / gridBoxWidth);

        console.table(`${percentage}%`);
        console.log(img.src);

        if (imageWidth > imageHeight) {
            img.parentNode.classList.add('right');
        } else if (percentage < 35 || imageWidth == imageHeight) {
            img.parentNode.classList.add('bottom-center');
        } else if (percentage > 35 && percentage < 50) {
            img.parentNode.classList.add('bottom');
        }
    })
}



window.addEventListener("load", imgPercentage);
window.addEventListener("resize", imgPercentage);

