// Logic inside of function
$(function () {
    $(window).on(`scroll load resize orientationchange`, function () {
        function addRemoveDiv() {
            // Window Width pointer
            var wW = $(window).width();

            // If window width is greater than or equal to 980 and div not created.
            if (wW >= 1200 && !$('.menu-all-wrap').length) {
                $(".menu-top, .navbar-brand, .navbar-toggler, .menu-collapse, .icon-menu-collapse").wrapAll('<div class="menu-all-wrap"></div>');
                // else if window is less than 980 and #newbox has been created.
            } else if (wW < 1200 && $('.menu-all-wrap').length) {
                $(".menu-top, .navbar-brand, .navbar-toggler, .menu-collapse, .icon-menu-collapse").unwrap();

            }
        }
        // Initial function call.
        addRemoveDiv();
        // On resize, call the function again
        $(window).on('resize', function () {
            addRemoveDiv();
        })
    });
});


$(document).ready(function () {
    $('.first-button').on('click', function () {
        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });
});


