$(document).ready(function () {
    if (document.getElementById('addToCart')) {
        let messageWrapper = document.getElementById('variations-message-wrapper')
        let message = document.getElementById('variations-message')
        let closeButton = document.getElementById('variations-close')

        let button = $('#addToCart')
        let observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.attributeName === 'class') {
                    let attributeValue = $(mutation.target).prop(mutation.attributeName)
                    if (attributeValue.includes('added')) {
                        messageWrapper.classList.add('active')
                        message.innerHTML =
                            `
                        Product has been added to your basket. You can continue shopping here,
                         <a href="https://baroncelli.redrocks.agency/cart/" class="variations-message-link">view basket</a>
                         or go
                         <a href="https://baroncelli.redrocks.agency/products/" class="variations-message-link">back to shop</a>.
                         `
                    } else if (attributeValue.includes('loading')) {
                        messageWrapper.classList.add('active')
                        message.innerHTML =
                            `
                        Please wait, Loading...
                         `
                    } else if (attributeValue.includes(' wc-variation-selection-needed')) {
                        messageWrapper.classList.add('active')
                        message.innerHTML =
                            `
                        Please select some product options before adding this product to your cart.
                         `
                    } else {
                        messageWrapper.classList.remove('active')
                    }
                }
            })
        })

        observer.observe(button[0], {
            attributes: true
        })

        closeButton.addEventListener('click', function () {
            messageWrapper.classList.remove('active')
        })
    }

})