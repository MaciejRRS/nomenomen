$(function () {
    $(window).on(`scroll load resize orientationchange`, function () {
        $('nav.navbar').addClass('active-menu');
        var wW = $(window).width();
        var wH = $(window).height();
        // console.log(wH);
        if (wW > 1440 && wH > 718) {
            if ($(window).scrollTop() <= wH - 71) {
                $('.active-menu').css('top', '-500px');
                $('header').css('display', 'block');
            } else {
                $('nav.navbar').css('background-color', 'white');
                $('.active-menu').css('top', '0px');
                $('header').css('display', 'block');
            }
        }
        else if (wW <= 1440) {
            if ($(window).scrollTop() <= 600) {
                $('.active-menu').css('top', '-500px');
                $('header').css('display', 'block');

            } else {
                $('nav.navbar').css('background-color', 'white');
                $('.active-menu').css('top', '0px');
                $('header').css('display', 'block');
            }
        }
    });
});




// scroll behavior for IOS
$(function () {
    $('a[href*=\\#]:not([href=\\#])').on('click', function () {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.substr(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    });
});



