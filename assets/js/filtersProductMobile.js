$(document).ready(function () {
    if (document.getElementById('wrap-params-item')) {

        const item = document.getElementById('wrap-params-item')
        const buttonOpen = document.getElementById('wrap-params-open')
        const buttonClose = document.getElementById('wrap-params-close')

        const changeFilterState = () => {
            if (item.classList.contains('active')) {
                item.classList.remove('active')
            } else {
                item.classList.add('active')
            }
        }

        buttonOpen.addEventListener('click', () => { changeFilterState() })

        buttonClose.addEventListener('click', () => { changeFilterState() })

    }
})
