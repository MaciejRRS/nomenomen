$(document).ready(function () {
var lastRotatedArrowFilter;
$('body').on('click', '.selectric-wrapper', function(e) {
    var spanFilter = $('.button', this);
    spanFilter.addClass('rotate-arrow');
    (lastRotatedArrowFilter && !lastRotatedArrowFilter[0].isSameNode(spanFilter[0])) && (lastRotatedArrowFilter
        .removeClass('rotate-arrow'));
    lastRotatedArrowFilter = spanFilter;
});
});