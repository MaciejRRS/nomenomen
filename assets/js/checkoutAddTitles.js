$(document).ready(function () {
    if(document.getElementById('billing_first_name_field') && !document.getElementsByClassName('woocommerce-account')){

        const addTitle = (id, text, style) => {
            let el = document.getElementById(id)
            let before = document.createElement("div")
            before.innerHTML = text
            before.style = style
            el.parentNode.insertBefore(before, el)
        }
    
        const style = `
            position: relative;
            grid-column-start: 1;
            grid-column-end: 4;
            padding: 15px 0;
            font-weight: 600;
        ` 
        
        addTitle('billing_first_name_field', 'PERSONAL DATA', style)
        addTitle('billing_address_1_field', 'ADDRESS', style)
        addTitle('billing_phone_field', 'CONTACT', style)
        
    }
})