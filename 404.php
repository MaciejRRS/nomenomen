<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

<section class="global-section">
    <main id="main" class="site-main" role="main">

        <div id="breadcrumbs" class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
            <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
        </div>



        <div class="container">





            <div style="color:#000000" class="text-container">
                <h1 style="text-align: center; font-size: 30px" class="mb-4">Error 404</h1>
            </div>
        </div>
    </main>
</section>

<?php get_footer(); ?>