<?php
/* 
Template Name: Strona Główna 
*/
?>

<?php get_header() ?>


<!-- JAVASCRIPT DO WYSZUKIWARKI -->


<main id="homesite">
    <section id="hero">
        <div class="container">
            <div class="flex">
                <img class="img" src="<?php the_field('hero_img') ?>">
                <div class="hero-content">
                    <div class="divider desctop" style="background-image: url(<?php the_field('hero_divider_line') ?>);"></div>
                    <h1 class="title"><?php the_field('hero_title') ?></h1>
                    <div class="divider mobile" style="background-image: url(<?php the_field('hero_divider_line') ?>);"></div>
                    <p class="subtitle"><?php the_field('hero_subtitle') ?></p>
                    <p class="text"><?php the_field('hero_text') ?></p>
                    <a class="link_full" href="<?php the_field('hero_button_link') ?>"><?php the_field('hero_button_text') ?></a>
                    <div class="divider" style="background-image: url(<?php the_field('hero_divider_line') ?>);"></div>
                </div>

            </div>
        </div>
    </section>
    <section id="grid">
        <div class="container">
            <h2 class="grid-title"><?php the_field('grid_title') ?></h2>
            <div class="grid">
                <?php while (have_rows('grid')) : the_row();
                ?>
                    <a href="<?php the_sub_field('link'); ?>" class="grid-section">
                        <img src="<?php the_sub_field('img'); ?>" />
                        <p class="grid-subtext"><?php the_sub_field('text'); ?></p>
                    </a>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
    <section id="about">
        <div class="container">
            <div class="flex">
                <h2 class="title mobile"><?php the_field('about_title') ?></h2>
                <img src="<?php the_field('about_img') ?>" />
                <div class="about_content">
                    <div class="about_img_first" style="background-image: url(<?php the_field('about_divider_line') ?>);"></div>
                    <h2 class="title desctop"><?php the_field('about_title') ?></h2>
                    <p class="text"><?php the_field('about_text') ?></p>
                    <div><a class="link_empty" href="<?php the_field('about_button_link') ?>"><?php the_field('about_button_text') ?></a></div>
                    <div class="about_img_second" style="background-image: url(<?php the_field('about_divider_line') ?>);"></div>
                </div>
            </div>
        </div>
    </section>
    <section id="bestsellers">
        <div class="container">
            <h2 class="bestsellers-title"><?php the_field('bestsellers_title') ?></h2>
            <ul class="products">
                <?php
                $args = array('post_type' => 'product', 'posts_per_page' => -1, 'orderby' => 'desc');
                $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();
                    global $product; ?>
                    <?php if (get_field('wlacz_widocznosc_produktu_na_stronie_glownej_sklepu_bestsellers')) { ?>
                        <li class="product">
                            <a href="<?php echo get_permalink($loop->post->ID) ?>">
                                <div class="imagewrapper">
                                    <?php if (has_post_thumbnail($loop->post->ID)) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog');
                                    else echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" width="300px" height="300px" />'; ?>
                                </div>
                                <div class="flex">
                                    <h3><?php the_title() ?></h3>
                                    <img class="divider" src="<?php the_field('line_divider_bestsellers') ?>" />
                                    <h4><?php echo $product->get_price_html(); ?></h4>
                                </div>
                            </a>
                        </li>
                    <?php } ?>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </ul>
            <div class="categories">
                <?php while (have_rows('bestsellers_categories')) : the_row();
                ?>
                    <a href="<?php the_sub_field('link'); ?>" class="categories-item">

                        <img src="<?php the_sub_field('img'); ?>" />
                        <p><?php the_sub_field('text'); ?></p>
                    </a>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
    <section id="collection">
        <div class="container">
            <div class="flex">
                <div class="about_content">
                    <div class="about_img_first" style="background-image: url(<?php the_field('collections_divider_line') ?>);"></div>
                    <h2 class="title desctop"><?php the_field('collections_title') ?></h2>
                    <p class="text"><?php the_field('collections_text') ?></p>
                    <div><a class="link_empty" href="<?php the_field('collections_button_link') ?>"><?php the_field('collections_button_text') ?></a></div>
                    <div class="about_img_second" style="background-image: url(<?php the_field('collections_divider_line') ?>);"></div>
                </div>
                <img src="<?php the_field('collections_img') ?>" />
                <h2 class="title mobile"><?php the_field('collections_title') ?></h2>
            </div>
        </div>
    </section>
    <section id="advise">
        <div class="container">
            <h2 class="advise-title"><?php the_field('advise_title') ?></h2>
            <ul class="products products_advise">


                <?php
                $args = array('post_type' => 'product', 'posts_per_page' => -1, 'orderby' => 'desc');
                $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();
                    global $product; ?>
                    <?php if (get_field('wlacz_widocznosc_produktu_na_stronie_glownej_sklepu_advise')) { ?>


                        <li class="product">
                            <a href="<?php echo get_permalink($loop->post->ID) ?>">

                                <div class="imagewrapper">
                                    <?php if (has_post_thumbnail($loop->post->ID)) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog');
                                    else echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" width="300px" height="300px" />'; ?>
                                </div>
                                <h3 class="title"><?php the_title() ?></h3>
                                <h4 class="price"><?php echo $product->get_price_html(); ?></h4>
                            </a>
                        </li>


                    <?php } ?>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>


            </ul>
        </div>
    </section>
    <section id="title-part">
        <div class="container">
            <h1 class="title-title"><?php the_field('title_title') ?></h1>
            <p><?php the_field('text_title') ?></p>
        </div>
    </section>
</main>
<?php get_footer(); ?>