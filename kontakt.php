<?php

/** Template Name: Kontakt
 **/
?>

<?php get_header(); ?>

<main id="kontakt">
    <div class="container">
        <div class="flex">
            <div class="links" style="background-image: url(<?php the_field('links_bg') ?>);">
                <h1> <?php the_field('title_kontakt') ?></h1>
                <?php

                if (have_rows('links_kontakt')) :
                    while (have_rows('links_kontakt')) : the_row();
                        $sub_value = get_sub_field('sub_field');
                ?>
                        <div class="flex">
                            <img src="<?php the_sub_field('icon'); ?>" alt="ikona posylania" />
                            <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('text'); ?></a>
                        </div>
                <?php
                    endwhile;
                endif;
                ?>

            </div>
            <div style="min-width: 300px; display: flex; flex-direction: column; justify-content: space-between; min-height: 600px">
                <img class="line" src="<?php the_field('dots_divider') ?>" />
                <div class="wyswig-content">
                    <?php the_field('right_text') ?>
                </div>
                <img class="line" src="<?php the_field('dots_divider') ?>" />
            </div class="annotation">
        </div>
    </div>
</main>

<?php get_footer(); ?>