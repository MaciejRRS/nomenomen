<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;


if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?>>
    <div class="wrapper_content mb-30">

        <div class="left-side">
            <div class="area-img-prod">
                <?php
                /**
                 * Hook: woocommerce_before_single_product_summary.
                 *
                 * @hooked woocommerce_show_product_sale_flash - 10
                 * @hooked woocommerce_show_product_images - 20
                 */
                do_action('woocommerce_before_single_product_summary');
                ?>
            </div>
        </div>


        <div class="right-side">
            <div class="opis-content">
                <div class="summary entry-summary">
                    <h1><?php echo the_title(); ?></h1>
                    <p class="category"><?php global $post, $product;
                                        $categ = $product->get_categories();
                                        echo $categ; ?></p>
                    <?php
                    /**
                     * Hook: woocommerce_single_product_summary.
                     *
                     * @hooked woocommerce_template_single_title - 5
                     * @hooked woocommerce_template_single_rating - 10
                     * @hooked woocommerce_template_single_price - 10
                     * @hooked woocommerce_template_single_excerpt - 20
                     * @hooked woocommerce_template_single_add_to_cart - 30
                     * @hooked woocommerce_template_single_meta - 40
                     * @hooked woocommerce_template_single_sharing - 50
                     * @hooked WC_Structured_Data::generate_product_data() - 60
                     */
                    do_action('woocommerce_single_product_summary');



                    ?>
                </div>
            </div>

            <div class="add-inform-area">
                <?php if (get_field('informacja_o_produkcie')) { ?>
                    <details open>
                        <summary class="title">
                            <h3>Informacje o produkcie</h3>
                            <img src="http://nomen.test/wp-content/uploads/2021/09/Path-816.svg" alt="open arrow" />
                        </summary>
                        <div class="content"><?php the_field('informacja_o_produkcie') ?></div>
                    </details>
                <?php } ?>
                <?php if (get_field('kupujac_pomagasz')) { ?>
                    <details>
                        <summary class="title">
                            <h3>Kupując pomagasz</h3>
                            <img src="http://nomen.test/wp-content/uploads/2021/09/Path-816.svg" alt="open arrow" />
                        </summary>
                        <div class="content"><?php the_field('kupujac_pomagasz') ?></div>
                    </details>
                <?php } ?>
                <?php if (get_field('dostawa')) { ?>
                    <details>
                        <summary class="title">
                            <h3>Dostawa</h3>
                            <img src="http://nomen.test/wp-content/uploads/2021/09/Path-816.svg" alt="open arrow" />
                        </summary>
                        <div class="content"><?php the_field('dostawa') ?></div>
                    </details>
                <?php } ?>
                <?php if (get_field('rozmiarowki')) { ?>
                    <details>
                        <summary class="title">
                            <h3>Rozmiarówka</h3>
                            <img src="http://nomen.test/wp-content/uploads/2021/09/Path-816.svg" alt="open arrow" />
                        </summary>
                        <div class="content"><?php the_field('rozmiarowki') ?></div>
                    </details>
                <?php } ?>
            </div>
        </div>



    </div>


    <div class="related-wrapper">
        <div class="container">
            <h2 class="title">Zobacz również</h2>
        </div>
    </div>
    <?php


    /**
     * Hook: woocommerce_after_single_product_summary.
     *
     * @hooked woocommerce_output_product_data_tabs - 30
     * @hooked woocommerce_upsell_display - 15 
     * @hooked woocommerce_output_related_products - 20
     */

    do_action('woocommerce_after_single_product_summary');
    ?>
</div>

<?php do_action('woocommerce_after_single_product'); ?>