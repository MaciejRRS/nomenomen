    <footer>
        <div class="container">
            <div class="line-divider" style="background-image: url(<?php the_field('line_divider_footer', 'option') ?>);" ></div>
            <div class="flex">
                <div class="footer-content">
                    <h3 class=""><?php the_field('text_contact_footer', 'option') ?></h3>
                    <div class="footer-line">
                        <img src="<?php the_field('phone_icon_footer', 'option') ?>" />
                        <a href="tel:+"><?php the_field('phone_contact_footer', 'option') ?></a>
                    </div>
                    <div class="footer-line">
                        <img src="<?php the_field('email_icon_footer', 'option') ?>" />
                        <a href="mailto:"><?php the_field('mail_contact_footer', 'option') ?></a>
                    </div>
                </div>
                <div class="footer-content">
                    <h3 class=""><?php the_field('text_social_footer', 'option') ?></h3>
                    <div class="footer-line">
                        <img src="<?php the_field('facebook_icon_footer', 'option') ?>" />
                        <a href=""><?php the_field('facebook_link_footer', 'option') ?></a>
                    </div>
                    <div class="footer-line">
                        <img src="<?php the_field('instagram_icon_footer', 'option') ?>" />
                        <a href=""><?php the_field('instagram_link_footer', 'option') ?></a>
                    </div>
                </div>
                <div class="footer-content">
                    <img src="<?php the_field('logo', 'option') ?>" alt="">
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
    </body>

    </html>