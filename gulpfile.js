
var imagesSRC = '../../uploads/2020/**/*.{png,PNG,jpg,JPG,jpeg,JPEG,gif,GIF,svg,SVG}'; // Source folder of images which should be optimized.
var imagesDestination = '../../opt/'; // Destination folder of optimized images. Must be different from the imagesSRC folder.



var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
const autoprefixer = require('gulp-autoprefixer');

// will auto - update browsers
function style() {
    return gulp.src(
        'assets/sass/main.scss',
        'assets/sass/*.scss',

    )
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream());
};

function styleShop() {
    return gulp.src(
        'assets/sass/shop.scss',
        'assets/sass/*.scss',
    )
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('shop.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream());
};

function styleHomepage() {
    return gulp.src(
        'assets/sass/homepage.scss',
        'assets/sass/*.scss',
    )
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('homepage.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream());
};



async function imgminif() {
    gulp.src('assets/images/*/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.mozjpeg({ quality: 75, progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({
                plugins: [
                    { removeViewBox: true },
                    { cleanupIDs: false }
                ]
            })
        ]))
};

async function jsminDef() {
    return gulp.src('assets/**/*.js')

        .pipe(jsmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist'));
};

function minifycss() {
    return gulp.src('assets/css/*.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist'));
};


function imgopt() {
    return gulp.src(imagesSRC)
        .pipe(imagemin({
            progressive: true,
            optimizationLevel: 3, // 0-7 low-high
            interlaced: true,
            svgoPlugins: [{ removeViewBox: false }]
        }))
        .pipe(gulp.dest(imagesDestination))
        .pipe(notify({ message: 'DONE: Images Optimized! 💯', onLast: true }));
};

function watch() {
    browserSync.init({
        proxy: {
            target: 'http://nomen.test/',
            ws: true
        },
        online: true,
    }
    );


    gulp.watch('./assets/sass/*.scss', style)
    gulp.watch('./assets/sass/*.scss', styleShop)
    gulp.watch('./assets/css/*.css', minifycss).on('change', browserSync.reload);
    gulp.watch('./assets/js/*.js', jsminDef).on('change', browserSync.reload);
    gulp.watch('./*.php').on('change', browserSync.reload);
    gulp.watch('./partials/*.php').on('change', browserSync.reload);
    gulp.watch('./woocommerce/**/*.php').on('change', browserSync.reload);
    gulp.watch('./search-filter/**/*.php').on('change', browserSync.reload);
    gulp.watch('./assets/js/*.js').on('change', browserSync.reload);
};




exports.style = style;
exports.styleShop = styleShop;
exports.styleHomepage = styleHomepage;
exports.imgopt = imgopt;
exports.minifycss = minifycss;
exports.watch = watch;
exports.imgminif = imgminif;
exports.jsminDef = jsminDef;
