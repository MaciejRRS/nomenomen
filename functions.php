<?php

require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';





// Register Custom Navigation Walker
add_theme_support('post-thumbnails', array('page'));

register_nav_menus(array(
    'primary' => __('Primary Menu', 'nomenomen'),
));


add_action('wp_enqueue_scripts', 'override_woo_frontend_styles');
function override_woo_frontend_styles()
{
    $file_general = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/baronceli/dist/shop.min.css';
    $whitelist = array(
        '127.0.0.1',
        '::1'
    );

    if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
        if (file_exists($file_general)) {
            wp_dequeue_style('woocommerce-general');
            wp_enqueue_style('woocommerce-general-custom', get_template_directory_uri() . '/dist/shop.min.css', array(), filemtime(get_template_directory() . '/dist/shop.min.css'));
        }
    } else {
        if (file_exists($file_general)) {

            wp_dequeue_style('woocommerce-general');
            wp_enqueue_style('woocommerce-general-custom', get_template_directory_uri() . '/assets/css/shop.css', array(), filemtime(get_template_directory() . '/assets/css/shop.css'));
        }
    }
}

function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

// Register thumbnails
add_theme_support('post-thumbnails');
add_image_size('homepage-thumb', 385, 302); // Soft Crop Mode

// This adds support for pages only:
add_theme_support('post-thumbnails', array('page'));

/*
* Enable support for custom logo.
*
*/
add_theme_support('custom-logo', array(
    'height'      => 400,
    'width'       => 100,
    'flex-height' => true,
    'flex-width'  => true,
));


//add option page to panel (ACF)

if (function_exists('acf_add_options_page')) {
    acf_add_options_page('Footer');
};




function shapeSpace_include_custom_jquery()
{
    global $template;
    $whitelist = array(
        '127.0.0.1',
        '::1'
    );
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/dist/js/jquery-3.5.1.min.js', array(), '3.5.1', false);
    wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/4abf0a1fd2.js', array('jquery'), null, true);

    if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
        wp_enqueue_style('style', get_template_directory_uri() . '/dist/styles.min.css', array(), filemtime(get_template_directory() . '/dist/styles.min.css'), 'all');
    } else {
        wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/styles.css', array(), filemtime(get_template_directory() . '/assets/css/styles.css'), 'all');
    }

    if ((basename($template) === 'single-product.php') || (basename($template) === 'archive-product.php') || (basename($template) === 'taxonomy-product-cat.php')) {
        wp_enqueue_script('variations-woo', get_template_directory_uri() . '/dist/js/selectric.min.js', array('jquery'), null, 'all', true);
        wp_enqueue_script('selectricinit', get_template_directory_uri() . '/dist/js/selectric-init.min.js', array(), filemtime(get_template_directory() . '/dist/js/selectric-init.min.js'), true);
        wp_enqueue_style('select2css', get_stylesheet_directory_uri() . '/dist/select2.min.css', array(), '4.0.6', 'all');
        wp_enqueue_script('select2js', get_template_directory_uri() . '/dist/js/select2.min.js', array('jquery'), '4.0.6', true);
        wp_enqueue_script('selectinit', get_template_directory_uri() . '/dist/js/select2-init.min.js', array('selectWoo'), filemtime(get_template_directory() . '/dist/js/select2-init.min.js'), true);
        wp_enqueue_script('gotovariations', get_template_directory_uri() . '/dist/js/goToVariations.min.js', array(), filemtime(get_template_directory() . '/dist/js/goToVariations.min.js'), true);
        wp_enqueue_script('filtersProductMobile', get_template_directory_uri() . '/dist/js/filtersProductMobile.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/filtersProductMobile.min.js'), true);
        wp_enqueue_script('paramsProductMobile', get_template_directory_uri() . '/dist/js/paramsProductMobile.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/paramsProductMobile.min.js'), true);
        wp_enqueue_script('addToCartAlert', get_template_directory_uri() . '/dist/js/addToCartAlert.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/addToCartAlert.min.js'), true);
    }

    if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
        wp_enqueue_script('wrapMenu', get_template_directory_uri() . '/dist/js/wrapMenu.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/wrapMenu.min.js'), true);
        wp_enqueue_script('footerMap', get_template_directory_uri() . '/dist/js/footerMap.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/footerMap.min.js'), true);
        wp_enqueue_script('arrowFooter', get_template_directory_uri() . '/dist/js/arrowFooter.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/arrowFooter.min.js'), true);
        wp_enqueue_script('gridCheck', get_template_directory_uri() . '/dist/js/gridCheck.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/gridCheck.min.js'), true);
        if ((basename($template) === 'single-product.php')) {
            wp_enqueue_script('arrowFooter', get_template_directory_uri() . '/dist/js/arrowShop.min.js', array('jquery'), null, 'all', true);
        }
    } else {
        wp_enqueue_script('wrapMenu', get_template_directory_uri() . '/assets/js/wrapMenu.js', array('jquery'), filemtime(get_template_directory() . '/assets/js/wrapMenu.js'), true);
        wp_enqueue_script('footerMap', get_template_directory_uri() . '/assets/js/footerMap.js', array('jquery'), filemtime(get_template_directory() . '/assets/js/footerMap.js'), true);
        wp_enqueue_script('arrowFooter', get_template_directory_uri() . '/assets/js/arrowFooter.js', array('jquery'), filemtime(get_template_directory() . '/assets/js/arrowFooter.js'), true);
        wp_enqueue_script('gridCheck', get_template_directory_uri() . '/dist/js/gridCheck.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/gridCheck.min.js'), true);
        wp_enqueue_script('dotsDelete', get_template_directory_uri() . '/dist/js/dotsDelete.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/dotsDelete.min.js'), true);
        wp_enqueue_script('checkoutAddTitles', get_template_directory_uri() . '/dist/js/checkoutAddTitles.min.js', array('jquery'), filemtime(get_template_directory() . '/dist/js/checkoutAddTitles.min.js'), true);

        if ((basename($template) === 'single-product.php')) {
            wp_enqueue_script('arrowFooter', get_template_directory_uri() . '/assets/js/arrowShop.js', array('jquery'), null, 'all', true);
        }
    }
}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');

function defer_parsing_of_js($url)
{
    if (is_user_logged_in()) return $url; //don't break WP Admin
    if (FALSE === strpos($url, '.js')) return $url;
    if (strpos($url, 'jquery-3.5.1.min.js')) return $url;
    return str_replace(' src', ' defer src', $url);
}
add_filter('script_loader_tag', 'defer_parsing_of_js', 10);


function add_rel_preload($html, $handle, $href, $media)
{

    if (is_admin())
        return $html;

    $html = <<<EOT
<link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'" id='$handle' href='$href' type='text/css' media='all' />
EOT;
    return $html;
}
add_filter('style_loader_tag', 'add_rel_preload', 10, 4);

remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10); // Remove title form original position
// remove image/thumbnail from products page 
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
// remove prive form products page
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

add_action('woocommerce_shop_loop_item_title', 'abChangeProductsTitle', 10); // Insert title in new position ( out of main container )
function abChangeProductsTitle()
{
    global $product;
    global $post;
    $pid = $product->get_id();
    $currency = get_woocommerce_currency_symbol();
    $collection = wp_get_post_terms($post->ID, 'collections');
    // if (!empty($collection)) {
    //     $collectionItem = $collection[0];
    //     $name = $collectionItem->name;
    // }
    $color = get_field('collection_colour', $collectionItem);
    $logo_collection = get_field('logo_collection', $collectionItem);
    $logo_collections_url = $logo_collection['url'];
    $category =  wp_get_post_terms($post->ID, 'product_cat');
    if (!empty($category)) {
        $categoryItem = $category[0];
        $nameCat = $categoryItem->name;
    }
    $low_price = $product->get_price_html();
    $from_price = get_field('from_price', 'options');
    $prefix = $from_price;
    $regular_simple_prod_price = $product->regular_price;
    $sale_simple_prod_price = $product->sale_price;
    echo '<div class="wrap-product">';
    echo '<div class="wrap-inside-link">';
    echo '<div class="background-color" data-color="' . $color . '" style="background-color:' . $color . '"></div>';
    echo '<div class="background-color-main"></div>';
    echo '<div class="img-area-product">';
    echo $product->get_image();
    echo '<div class="wrap-price">';
    echo '<div class="wrap-image-name">';
    echo '<img class="image-collection" src="' . $logo_collections_url . '" alt="">';
    echo '<div class="flex">';
    echo '<h3 class="wrap-price__product-title-hp">';
    echo '' . the_title() . '';
    echo '</h3>';
    if ($product->is_type('simple')) {
            if ($product->regular_price && !$sale_simple_prod_price) {
                echo '<div class="wrap-regular-price"> ' . wc_price($regular_simple_prod_price) . '</div>';
            } else {
                echo '<div class="warp-reg-price"><del>' . wc_price($regular_simple_prod_price) . '</del>' . '<ins> ' . wc_price($sale_simple_prod_price) . '</ins></div>';
            }
        } elseif ($product->is_type('variable')) {
            echo '<div class="warp-reg-price">' . $low_price . '</div>';
        };
    echo '</div>';
    echo '</div>';
    echo '<div class="wrap-sizes-prices">';
    echo '<p class="size">' . $product->get_attribute('size') . '</p>';
    echo '</div>';
    echo '</div>';
    echo  '</div>';
    echo '</div>';
    echo '</div>';
    echo '<div class="wrap-info-product-mobile">';
    echo '<div class="Text-info-product-mobile">';
    echo '<div class="Text-productAndCollectionWrap">';
    echo '</div>';
    echo '</div>';
    echo '<div class="background-color" data-color="' . $color . '" style="background-color:' . $color . '">';
    echo '<img class="image-collection" src="' . $logo_collections_url . '" alt="">';
    echo '</div>';
    echo '</div>';
};

// deleted add to cart button from collections and category page
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10); // Remove title form original position


add_filter('woocommerce_sale_flash', 'ds_change_sale_text');

function ds_change_sale_text() {
return '<span class="onsale">%</span>';
}



remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 6 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
?>